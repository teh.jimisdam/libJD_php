<?php

namespace libJD_php\Templating;


use libJD_php\Collections\Collection;

class TemplateEngine
{
    /** @var Collection $template_engines */
    private static $template_engines;
    /** @var array $template_titles */
    private $template_titles;


    /**
     * TemplateEngine constructor.
     * @param string $title
     * @param array $template_titles
     */
    public function __construct(string $title, array $template_titles = [])
    {
        $this->template_titles = $template_titles;
        static::$template_engines[$title] = &$this;
    }

    /**
     * @param string $title
     * @return TemplateEngine
     */
    public static function &getTemplateEngine(string $title): TemplateEngine
    {
        return self::$template_engines[$title];
    }

    /**
     * @param string $content
     * @param Collection $data
     * @return string
     */
    public function render(string $content, Collection &$data): string
    {
        foreach ($this->template_titles as $template_title)
            $content = Template::getTemplate($template_title)->render($content, $data);

        return $content;
    }


}