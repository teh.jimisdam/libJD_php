<?php

namespace libJD_php\Templating;


use libJD_php\Collections\Collection;

interface Renderable
{
    /**
     * @param string $content
     * @param Collection $data
     * @return string
     */
    public function render(string $content, Collection &$data): string;
}