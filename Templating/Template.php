<?php

namespace libJD_php\Templating;


use libJD_php\Collections\Collection;

class Template implements Renderable
{
    /** @var Collection $templates */
    private static $templates;
    /** @var callable $template_function */
    private $template_function;


    /**
     * Template constructor.
     * @param string $title
     * @param callable $template_function
     */
    public function __construct(string $title, callable $template_function)
    {
        $this->template_function = $template_function;
        static::$templates[$title] = &$this;
    }

    /**
     * @param string $title
     * @return Template
     */
    public static function &getTemplate(string $title): Template
    {
        return self::$templates[$title];
    }

    /**
     * @param string $content
     * @param Collection $data
     * @return string
     */
    public function render(string $content, Collection &$data): string
    {
        return ($this->template_function)($content, $data);
    }
}