<?php

namespace libJD_php\Templating;


use libJD_php\Collections\Collection;

class Views
{
    /** @var string $root_directory */
    private static $root_directory;


    /**
     * @param string $view
     * @param string $template_engine
     * @param Collection $data
     */
    public static function render(string $view, string $template_engine, Collection &$data)
    {
        $view_info = [];
        if (strpos($view, '#'))
            $view_info['layout'] = explode('#', $view)[0];

        $view_info['path'] =
            preg_replace("#\.#", '/',
                isset($view_info['layout']) ? explode('#', $view)[1] : $view
            );

        $view_content = file_get_contents(static::$root_directory . "/{$view_info['path']}.php");

        if (isset($view_info['layout']))
            $view_content =
                preg_replace(
                    "#" . addslashes('~yield~') . "#",
                    $view_content,
                    file_get_contents(static::$root_directory . "/layouts/${view_info['layout']}.php")
            );

        $view_content = TemplateEngine::getTemplateEngine($template_engine)->render($view_content, $data);
        file_put_contents(static::$root_directory . "/Temp/tmp.php", $view_content);
        include static::$root_directory . "/Temp/tmp.php";
        unlink(static::$root_directory . "/Temp/tmp.php");
    }

    /**
     * @param string $root_directory
     */
    public static function setRootDirectory(string $root_directory)
    {
        static::$root_directory = $root_directory;
    }
}