<?php

namespace libJD_php\Routing;


use libJD_php\Collections\Collection;

class Route
{
    private $path;
    private $method;
    /** @var array $request */
    private $request;
    /** @var callable|string $action */
    private $action;
    /** @var string[] $middleware */
    private $middleware;

    /**
     * Route constructor.
     * @param string $path
     * @param string $method
     * @param callable|string|null $action
     * @param string[]|null $middleware
     */
    public function __construct(string $path, string $method, $action = null, array $middleware = null)
    {
        $this->path = $path;
        $this->method = $method;
        $this->request = ['path' => $_SERVER['REQUEST_URI'], 'method' => $_SERVER['REQUEST_METHOD']];
        $this->action = $action;
        $this->middleware = $middleware or [];
    }

    /**
     * @return Collection
     */
    private function parametrize()
    {
//        if ($this->request['method'] != 'GET')
//            return new Collection($_POST);

        $path_split = preg_split('#/#', $this->path);
        $request_split = preg_split('#/#', $this->request['path']);

        $params = new Collection();
        for ($i = 0; $i < count($path_split); $i++)
            if ($path_split[$i][0] == ':')
                $params[substr($path_split[$i], 1)] = $request_split[$i + 1];

        return $params;
    }

    /**
     * @return bool
     */
    public function match()
    {
        return
            $this->method == $this->request['method']
            and
            preg_match('#^(/|)' . preg_replace("#:\w+#", "(\w+)", $this->path) . '(/|)$#', $this->request['path']);
    }

    public function run()
    {
        $params = empty($this->parametrize()->toArray()) ? null : $this->parametrize();

        if(!empty($this->middleware))
            foreach ($this->middleware as $middleware)
                ("{$middleware}Middleware::handle")($params);

        if (is_callable($this->action))
            ($this->action)($params);

        if (is_string($this->action))
        {
            $controller = preg_split('/#/', $this->action);
            ("$controller[0]Controller::$controller[1]")($params);
        }

    }
}
