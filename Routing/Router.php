<?php

namespace libJD_php\Routing;


class Router
{
    /** @var Route[] $routes */
    private $routes;


    /**
     * Router constructor.
     * @param Route[] $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    public function listen()
    {
        foreach ($this->routes as &$route)
            if ($route->match())
                $route->run();
    }

}