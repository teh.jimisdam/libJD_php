<?php

namespace libJD_php\Collections;


use ArrayObject;

class Collection extends ArrayObject
{
    /**
     * @param $key
     * @param null $value
     * @return $this
     */
    public function set($key, $value = null)
    {
        $this[$key] = $value;

        return $this;
    }

    /**
     * @param mixed $index
     * @return Collection|mixed
     */
    public function &offsetGet($index)
    {
        return is_array(parent::offsetGet($index)) ? new Collection(parent::offsetGet($index)) : parent::offsetGet($index);
    }

    /**
     * @param $data
     * @return $this
     */
    public function push($data)
    {
        parent::append($data);

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this[$key];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }

    /**
     * @return string
     */
    public function toJSON()
    {
        return json_encode($this);
    }

    /**
     * @param $key
     * @return bool
     */
    public function containsKey($key)
    {
        return isset($this[$key]);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function findValue($value)
    {
        $data = new Collection(['value' => $value, 'results' => new Collection()]);
        foreach ($this as $aKey => &$aValue)
            if ($aValue == $data['value'])
                $data['results']->push($aKey);

        return $data['results']->toArray();
    }
}