<?php

namespace libJD_php\Databases;


use libJD_php\Collections\Collection;
use PDO;

abstract class Database
{
    /** @var static[] $databases */
    private static $databases = [];
    /** @var PDO $pdo */
    protected $pdo;
    /** @var array $info */
    protected $info;


    /**
     * Database constructor.
     * @param $database
     * @param $username
     * @param $password
     * @param string $address
     * @param int $port
     */
    public function __construct($database, $username, $password, $address='localhost', int $port)
    {

        $this->info['database']     = $database;
        $this->info['username']     = $username;
        $this->info['password']     = $password;
        $this->info['address']      = $address;
        $this->info['port']         = $port;

        self::$databases[$database] = &$this;
    }

    /**
     * @param string $database
     * @return Database|null
     */
    public static function &getDatabase(string $database): self
    {
        return self::$databases[$database];
    }

    public abstract function connect();

    public function disconnect()
    {
        $this->pdo = null;
    }

    /**
     * @param string $query
     * @param Collection|null $params
     * @return Collection
     */
    public abstract function query(string $query, Collection $params=null): Collection;
}