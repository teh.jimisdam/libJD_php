<?php

namespace libJD_php\Databases;


use libJD_php\Collections\Collection;
use PDO;

class MySQL_Driver extends Database
{
    public function __construct($database, $username, $password, $address='localhost', $port=3306)
    {
        parent::__construct($database, $username, $password, $address, $port);
    }

    public function connect()
    {
        $this->pdo = new PDO("mysql:dbname={$this->info['database']};host={$this->info['address']};port={$this->info['port']}", $this->info['username'], $this->info['password']);
    }

    /**
     * @param string $query
     * @param Collection|null $params
     * @return Collection
     */
    public function query(string $query, Collection $params=null): Collection
    {
        $params = !$params ? new Collection() : $params;
        $parametrized = new Collection();

        foreach ($params as $key => &$value)
            $parametrized[":$key"] = $value;
        $statement = $this->pdo->prepare($query);

        return
            (new Collection())
                ->set('status', $statement->execute($parametrized->toArray()))
                ->set('results', $statement->fetchAll(PDO::FETCH_ASSOC));
    }
}
