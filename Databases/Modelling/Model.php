<?php

namespace libJD_php\Databases\Modelling;


use libJD_php\Collections\Collection;
use libJD_php\Databases\Database;

abstract class Model
{
    /** @var Database $database */
    private static $database;

    /** @var array $information */
    private $information = [];


    public function __get($name)
    {
        return isset($this->information[$name]) ? $this->information[$name] : null;
    }

    public function __set($name, $value)
    {
        if (isset($this->information[$name]))
            $this->information[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->information[$name]);
    }


    public function __construct(array $information)
    {
        $this->information = $information;
    }

    public function save(): bool
    {
        $return = 0;
        if (!isset($this->id))
            $return = self::create($this->information);
        else
        {
            $sets = "";
            foreach ($this->information as $key => &$value)
                $sets .= "`$key`=\"" . addslashes($value) . "\", ";
            $sets = substr($sets, 0, strlen($sets) - 2);

            self::$database->connect();
            $return = self::$database->query("UPDATE " . static::class . " SET $sets WHERE id=$this->id")['status'];
            self::$database->disconnect();
        }

        return $return;
    }

    public function destroy(): bool
    {
        self::$database->connect();
        $return = self::$database->query("DELETE FROM " . static::class . " WHERE id=$this->id")['status'];
        self::$database->disconnect();

        return $return;
    }


    /**
     * @param Database $database
     */
    public static function setDatabase(Database &$database)
    {
        self::$database = &$database;
    }

    public static function create(array $information): bool
    {
        self::$database->connect();

        $columns = self::$database->query("SHOW COLUMNS FROM " . static::class)['results'];
        unset($columns[0]);
        foreach ($columns as &$column) $column = $column['Field'];

        foreach ($columns as &$column)
            if (!isset($information[$column]))
                return false;

        $tmp = "";
        foreach ($columns as &$column) $tmp .= "`$column`, ";
        $columns = '(' . substr($tmp, 0, strlen($tmp) - 2) . ')';

        $tmp = "";
        foreach ($information as &$datum) $tmp .= "\"" . addslashes($datum) . "\", ";
        $information = '(' . substr($tmp, 0, strlen($tmp) - 2) . ')';

        $tmp = self::$database->query("INSERT INTO " . static::class . "$columns VALUES $information");
        self::$database->disconnect();

        return $tmp['status'];
    }

    public static function getBy(...$criteria): Collection
    {
        $criteria_fixed = "";
        foreach ($criteria as $criterion)
            $criteria_fixed .= preg_replace('/(\w+)/A', '`$0`', $criterion) . " AND ";
        $criteria_fixed = substr($criteria_fixed, 0, strlen($criteria_fixed) - 4);

        self::$database->connect();
        $query = self::$database->query("SELECT * FROM " . static::class . (!empty($criteria_fixed) ? " WHERE " . $criteria_fixed : ""));
        self::$database->disconnect();

        $results = &$query['results'];
        if (!empty($results))
            foreach ($results as &$result)
                $result = new static($result);

        return new Collection($results);
    }
}